function VeinScanDrawing(id, labels) {
  var data = {
    svg: { element: null, scale: null, canvas: null, width: null, height: null, id: null },
    interfaceButtons: { element: null, selected: { pencil: true, eraser: false }, history: [], horizontalMargin: 7, topMargin: 65 },
    labels: { element: false, offset: null, right: [], left: [], rightColor: '#e7f3a3', leftColor: '#1eac00', showDiameter: false },
    events: { isDragging: false },
    drawing: {
      currentPath: null,
      initialPoint: null,
      drawnCounter: -1,
      erasedCounter: -1,
      buffer: null,
      bufferSize: 8,
      strokeColor: '#888', // #888 || #fff
      strokeWidth: 2, // 2 || 4
    },
  }

  data.svg.element = document.getElementById(id).children[0];
  data.svg.id = id

  if (Array.isArray(labels.right)) {
    data.labels.right = labels.right;
  }
  if (Array.isArray(labels.left)) {
    data.labels.left = labels.left
  }
  if (typeof labels.showDiameter === "boolean") {
    data.labels.showDiameter = labels.showDiameter
  }

  window.addEventListener('load', function (e) {
    init();
    debug()
  })

  function debug() {
    document.getElementById('png-button').addEventListener('click', renderPNG)
  }

  function init() {

    computeSVGScale()

    setupInterface()

    // add event listeners
    data.svg.element.addEventListener('mousemove', mouseMove)
    data.svg.element.addEventListener('mousedown', mouseDown)
    data.svg.element.addEventListener('mouseup', mouseUp)
    window.addEventListener('resize', documentResize)
  }

  function getSVGChild(c) {
    return document.querySelector('#' + data.svg.id + ' .' + c)
  }

  function setupInterface() {
    // set the default cursor
    data.svg.element.classList.add('pencil-cursor');

    // dashed border
    var rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
    rect.setAttribute('x', '0')
    rect.setAttribute('y', '0')
    rect.setAttribute('width', '100%')
    rect.setAttribute('height', '100%')
    rect.setAttribute('fill', 'none')
    rect.setAttribute('stroke', '#555')
    rect.setAttribute('stroke-dasharray', '2,2')
    rect.setAttribute('stroke-width', '2')

    getSVGChild('user-interface-group').appendChild(rect)

    // pencil icon
    var t1 = 'scale(0.65) translate(130,80)'
    var p1 = `M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z`
    var pencil = createInterfaceButton('pencil-button normal-cursor', true, 0, t1, p1)

    // eraser icon
    var t2 = 'scale(0.7) translate(80,90)'
    var p2 = `M497.941 273.941c18.745-18.745 18.745-49.137 0-67.882l-160-160c-18.745-18.745-49.136-18.746-67.883 0l-256 256c-18.745 18.745-18.745 49.137 0 67.882l96 96A48.004 48.004 0 0 0 144 480h356c6.627 0 12-5.373 12-12v-40c0-6.627-5.373-12-12-12H355.883l142.058-142.059zm-302.627-62.627l137.373 137.373L265.373 416H150.628l-80-80 124.686-124.686z`
    var eraser = createInterfaceButton('eraser-button', false, 1, t2, p2)

    // undo icon
    var t3 = 'scale(0.65) translate(160,120)'
    var p3 = `M212.333 224.333H12c-6.627 0-12-5.373-12-12V12C0 5.373 5.373 0 12 0h48c6.627 0 12 5.373 12 12v78.112C117.773 39.279 184.26 7.47 258.175 8.007c136.906.994 246.448 111.623 246.157 248.532C504.041 393.258 393.12 504 256.333 504c-64.089 0-122.496-24.313-166.51-64.215-5.099-4.622-5.334-12.554-.467-17.42l33.967-33.967c4.474-4.474 11.662-4.717 16.401-.525C170.76 415.336 211.58 432 256.333 432c97.268 0 176-78.716 176-176 0-97.267-78.716-176-176-176-58.496 0-110.28 28.476-142.274 72.333h98.274c6.627 0 12 5.373 12 12v48c0 6.627-5.373 12-12 12z`
    var undo = createInterfaceButton('undo-button normal-cursor', false, 2, t3, p3)

    getSVGChild('user-interface-group').appendChild(pencil)
    getSVGChild('user-interface-group').appendChild(eraser)
    getSVGChild('user-interface-group').appendChild(undo)

    drawLabels()
  }

  /*
    Label color legend SVG Group
  */
  function createLabelLegend(side) {
    var offsetX = 0; var offsetY = data.interfaceButtons.topMargin;
    var color = ''

    if (side === 'left') {
      offsetX = data.interfaceButtons.horizontalMargin;
      color = data.labels.leftColor;
    } else if (side === 'right') {
      offsetX = (data.svg.width - data.interfaceButtons.horizontalMargin) - 26;
      color = data.labels.rightColor;
    }

    var legend = document.createElementNS('http://www.w3.org/2000/svg', 'g')
    legend.setAttribute('transform', 'translate(' + offsetX + ',' + offsetY + ')')

    var r1 = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
    r1.setAttribute('fill', color)
    r1.setAttribute('stroke', '#000')
    r1.setAttribute('stroke-width', '0.1')
    r1.setAttribute('width', '4')
    r1.setAttribute('height', '4')

    var tx1 = document.createElementNS('http://www.w3.org/2000/svg', 'text')
    tx1.setAttribute('font-size', '5px')
    tx1.setAttribute('fill', '#000')
    tx1.setAttribute('x', '6')
    tx1.setAttribute('y', '4')
    tx1.textContent = side.charAt(0).toUpperCase() + side.slice(1);
    tx1.setAttribute('class', 'unselectable-text-group')
    legend.appendChild(r1)
    legend.appendChild(tx1)

    return legend
  }

  function createInterfaceButton(buttonClass, selected, index, transform, path) {
    var g1 = document.createElementNS('http://www.w3.org/2000/svg', 'g')
    g1.setAttribute('transform', 'translate(5,' + (20 * index) + ') scale(0.06)')
    g1.setAttribute('class', 'drawing-button')

    var svg1 = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    svg1.setAttribute('class', buttonClass)
    svg1.setAttribute('viewBox', '0 0 512 512')
    svg1.setAttribute('preserveAspectRatio', 'xMidYMid meet')
    svg1.setAttribute('fill', (selected === true ? '#baf7ff' : '#ccc'))

    var g2 = document.createElementNS('http://www.w3.org/2000/svg', 'g')
    var r2 = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
    r2.setAttribute('x', '10')
    r2.setAttribute('y', '5')
    r2.setAttribute('width', '484')
    r2.setAttribute('height', '484')
    r2.setAttribute('stroke', '#000')
    r2.setAttribute('ry', '143.23')
    r2.setAttribute('stroke-width', '15')
    g2.appendChild(r2)

    var g3 = document.createElementNS('http://www.w3.org/2000/svg', 'g')
    g3.setAttribute('transform', transform)
    g3.setAttribute('fill', '#000')

    var p1 = document.createElementNS('http://www.w3.org/2000/svg', 'path')
    p1.setAttribute('d', path)

    g3.appendChild(p1)
    svg1.appendChild(g2)
    svg1.appendChild(g3)
    g1.appendChild(svg1)
    return g1
  }

  function createLabel(index, side, transform = null, diameter = '') {
    if (typeof index !== 'number' || !(side === 'right' || side === 'left')) {
      throw new Error(`Bad arguments!`)
    }

    data.labels[side][index].element.addEventListener('label-diameter-update', labelDiameterEvent)

    var str = data.labels[side][index].acronym;

    // calculate label size based on text character size
    var characterWidth = 5.5;
    var rectHeight = 12;
    var rectWidth = (str.length * characterWidth)
    rectWidth += 10; // right-side padding

    if (data.labels.showDiameter) {
      rectHeight = 20
    }

    var labelBackgroundColor = '';
    var drawingHorizontalMargin = data.interfaceButtons.horizontalMargin;  // pixels
    var drawingVerticalMargin = data.interfaceButtons.topMargin; // pixels, needs a margin because of the user-interface buttons
    var xOffset = 0; var yOffset = ((index * 30) + 10) + drawingVerticalMargin;

    // error check for too many labels
    if (yOffset > data.svg.height) {
      throw new Error(`Too many labels! Does not fit in drawing!`)
    }

    if (side === 'right') {
      xOffset = (data.svg.width - drawingHorizontalMargin) - rectWidth;
      labelBackgroundColor = data.labels.rightColor;
    } else if (side === 'left') {
      xOffset = drawingHorizontalMargin;
      labelBackgroundColor = data.labels.leftColor;
    }

    if (!transform) {
      transform = 'translate(' + xOffset + ',' + yOffset + ')'
    }

    var group = document.createElementNS('http://www.w3.org/2000/svg', 'g')
    group.setAttribute('class', 'draggable-label draggable-cursor unselectable-text-group')
    group.setAttribute('transform', transform)
    group.setAttribute('data-acronym', str.toLowerCase())
    group.setAttribute('data-side', side)
    group.setAttribute('data-diameter', '')

    var rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
    rect.setAttribute('x', '0')
    rect.setAttribute('y', '0')
    rect.setAttribute('width', rectWidth + 'px')
    rect.setAttribute('height', rectHeight + 'px')
    rect.setAttribute('fill', labelBackgroundColor)
    rect.setAttribute('stroke', '#000')
    rect.setAttribute('stroke-dasharray', '0')
    rect.setAttribute('stroke-width', '0.6')

    var text = document.createElementNS('http://www.w3.org/2000/svg', 'text')
    text.setAttribute('x', '5')
    text.setAttribute('y', '8')
    text.setAttribute('pointer-events', 'none')
    text.setAttribute('fill', '#000')
    text.setAttribute('font-family', "'Courier New', Courier, monospace")
    text.setAttribute('font-size', '9px')
    text.textContent = str

    var text2 = document.createElementNS('http://www.w3.org/2000/svg', 'text')
    text2.setAttribute('font-size', '5px')
    text2.setAttribute('font-family', 'sans-serif')
    text2.setAttribute('fill', '#000')
    text2.setAttribute('class', 'label-diameter-text')
    text2.textContent = diameter

    group.appendChild(rect)
    group.appendChild(text)
    group.appendChild(text2)

    return group
  }

  /*
    Calculate what 'font-size' will fit in requested width.
    Assumes pixel units.

    @param {Float} containerWidth
    @param {Float} startingFontSize
    @param {String} text
    @return {Float}
  */
  function calculateConstrainedFontSize(containerWidth, fontFamily, startingFontSize, text) {
    // create <svg> and <text> elements.
    var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    var width = 1000
    var height = 300
    svg.setAttribute('viewBox', '0 0 ' + width + ' ' + height) // really big
    svg.setAttribute('width', width + 'px')
    svg.setAttribute('height', height + 'px')
    svg.setAttribute('visibility', 'hidden')

    // append it to the body, so that bounds are calculated, otherwise returns `{x: 0, ....}`
    document.body.appendChild(svg)

    var t = document.createElementNS('http://www.w3.org/2000/svg', 'text')
    t.setAttribute('fill', '#000')
    t.setAttribute('x', '0')
    t.setAttribute('y', '0')
    t.setAttribute('font-family', fontFamily)
    t.setAttribute('font-size', startingFontSize + 'px')
    t.textContent = text

    svg.appendChild(t)

    // loop to find the font-size
    var fontSize = startingFontSize;
    var bounds = { height: 0, width: 0 };

    while (fontSize > 0) {
      fontSize -= 0.1;
      t.setAttribute('font-size', fontSize + 'px')
      bounds = t.getBoundingClientRect()
      if (bounds.width <= containerWidth) {
        break
      }
    }

    if (fontSize <= 0) {
      throw new Error('Container is too small. Font-size cannot be zero!')
    }

    // remove elements, finished calculating
    document.body.removeChild(svg)

    return {
      size: fontSize,
      height: bounds.height,
      width: bounds.width,
    }
  }

  function centerTextHorizontal(containerWidth, textWidth) {
    return (containerWidth - textWidth) / 2
  }

  function labelDiameterEvent(e) {
    if (!e.detail.acronym || !e.detail.side) {
      throw new Error('Custom event is missing attributes!')
    }

    var labelGroup = getSVGChild('labels-group')

    // search for matching child element
    for (var i = 0; i < labelGroup.children.length; i++) {
      var child = labelGroup.children[i];
      if (child.dataset.acronym === e.detail.acronym && child.dataset.side === e.detail.side) {
        child.dataset.diameter = e.detail.diameter

        // rectangle element
        var bounds = {
          width: child.children[0].width.baseVal.value,
          height: child.children[0].height.baseVal.value
        }
        var f = calculateConstrainedFontSize(bounds.width - 3, 'san-serif', 5, e.detail.diameter)
        var pos = {
          x: centerTextHorizontal(bounds.width, f.width),
          y: bounds.height - 5,
        }

        // second text element, diameter label
        child.children[2].textContent = e.detail.diameter
        child.children[2].setAttribute('font-size', f.size + 'px')
        child.children[2].setAttribute('transform', 'translate(' + pos.x + ',' + pos.y + ')')
      }
    }
  }

  function drawLabels() {
    var labelGroup = getSVGChild('labels-group')

    for (var i = 0; i < data.labels.left.length; i++) {
      var group = createLabel(i, 'left')
      labelGroup.appendChild(group)
    }

    for (var j = 0; j < data.labels.right.length; j++) {
      var group = createLabel(j, 'right')
      labelGroup.appendChild(group)
    }

    // label color legend
    var leftLegend = createLabelLegend('left');
    var rightLegend = createLabelLegend('right');

    getSVGChild('user-interface-group').appendChild(leftLegend)
    getSVGChild('user-interface-group').appendChild(rightLegend)
  }

  function computeSVGScale() {
    // calculate svg viewBox scale
    // other attribute 'currentScale' refers to <svg transform="scale(2)">
    var svgRect = data.svg.element.getBoundingClientRect()
    data.svg.scale = {}
    data.svg.scale.width = (svgRect.width / data.svg.element.viewBox.baseVal.width);
    data.svg.scale.height = (svgRect.height / data.svg.element.viewBox.baseVal.height);

    data.svg.width = data.svg.element.viewBox.baseVal.width;
    data.svg.height = data.svg.element.viewBox.baseVal.height;
  }

  function eventPositionToSVGPosition(e) {
    var svgRect = data.svg.element.getBoundingClientRect()
    var newX = ((e.clientX - svgRect.x) / data.svg.scale.width)
    var newY = ((e.clientY - svgRect.y) / data.svg.scale.height)

    return { x: newX, y: newY }
  }

  function appendToPathBuffer(pt) {
    if (Array.isArray(data.drawing.buffer)) {
      data.drawing.buffer.push(pt)
      while (data.drawing.buffer.length > data.drawing.bufferSize) {
        data.drawing.buffer.shift()
      }
    }
  }

  // taken from: https://stackoverflow.com/questions/40324313/svg-smooth-freehand-drawing
  function getAveragePoint(offset) {
    var len = data.drawing.buffer.length;
    if (len % 2 === 1 || len >= data.drawing.bufferSize) {
      var totalX = 0;
      var totalY = 0;
      var pt, i;
      var count = 0;
      for (i = offset; i < len; i++) {
        count++;
        pt = data.drawing.buffer[i];
        totalX += pt.x;
        totalY += pt.y;
      }
      return {
        x: totalX / count,
        y: totalY / count
      }
    }
    return null;
  }

  function updateDrawingPath() {
    var pt = getAveragePoint(0);

    var path = document.getElementById(data.drawing.currentPath)
    var strPath = path.getAttribute('d')

    if (pt) {
      // Get the smoothed part of the path that will not change
      strPath += " L" + pt.x + " " + pt.y;

      // Get the last part of the path (close to the current mouse position)
      // This part will change if the mouse moves again
      var tmpPath = "";
      for (var offset = 2; offset < data.drawing.buffer.length; offset += 2) {
        pt = getAveragePoint(offset);
        tmpPath += " L" + pt.x + " " + pt.y;
      }

      var path = document.getElementById(data.drawing.currentPath)
      // Set the complete current path coordinates
      path.setAttribute("d", strPath + tmpPath);
    }
  }

  function createAndInsertPath(event) {
    var p = eventPositionToSVGPosition(event)

    if (data.interfaceButtons.selected.pencil) {
      data.drawing.drawnCounter += 1;
      data.drawing.currentPath = data.svg.id + '-drawn-path-' + data.drawing.drawnCounter
    } else if (data.interfaceButtons.selected.eraser) {
      data.drawing.erasedCounter += 1;
      data.drawing.currentPath = data.svg.id + '-erased-path-' + data.drawing.erasedCounter
    }
    data.drawing.initialPoint = { x: p.x, y: p.y };
    data.drawing.buffer = []
    appendToPathBuffer(p)

    // create a path, get x and y
    var pathStartingPoint = "M" + p.x + "," + p.y
    var path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    path.setAttribute('d', pathStartingPoint)
    path.setAttribute('fill', 'none')
    path.setAttribute('stroke', data.drawing.strokeColor)
    path.setAttribute('stroke-width', data.drawing.strokeWidth + 'px')
    path.setAttribute('id', data.drawing.currentPath)
    path.setAttribute('class', 'user-drawn-path')
    getSVGChild('drawn-group').appendChild(path)
  }

  /**
    * Asynchronous function. Renders an SVG to a PNG.
    * Will not work in Internet explorer 11 or below.
    * 
    * Reference: https://stackoverflow.com/questions/27230293/how-to-convert-svg-to-png-using-html5-canvas-javascript-jquery-and-save-on-serve
    * 
    * @async
    * @param {Object} svg - The SVG Javascript Object, not the raw HTML/XML string
    * @returns {Promise<string>} Resolver function takes one argument -  The Data URL / Base64 string of the PNG file.
    * 
  */
  function svgToPNG(svg) {
    return new Promise(function (resolve, reject) {

      window.URL = window.URL || window.webkitURL;

      var xml = new XMLSerializer().serializeToString(svg)
      var width = svg.width.baseVal.value
      var height = svg.height.baseVal.value

      var file = new Blob([xml], { type: "image/svg+xml;charset=utf-8" })

      var image = new Image()
      image.src = window.URL.createObjectURL(file)
      image.onload = function () {
        var canvas = document.createElement('canvas')
        canvas.width = width
        canvas.height = height
        var ctx = canvas.getContext('2d')
        ctx.drawImage(image, 0, 0)
        resolve(canvas.toDataURL('image/png'))
      }
      image.onerror = function (e) {
        reject(e)
      }

    })
  }

  function cloneDrawing(scale = 1) {
    // legs
    var parentSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    parentSVG.setAttribute('width', (data.svg.width * scale) + 'px')
    parentSVG.setAttribute('height', (data.svg.height * scale) + 'px')
    parentSVG.setAttribute('viewBox', '0 0 ' + data.svg.width + ' ' + data.svg.height)
    
    // background color
    parentSVG.appendChild(getSVGChild('background-group').cloneNode(true))

    // drawn paths group
    parentSVG.appendChild(getSVGChild('drawn-group').cloneNode(true))

    // background svg image
    parentSVG.appendChild(getSVGChild('background-image').cloneNode(true))

    // labels group
    parentSVG.appendChild(getSVGChild('labels-group').cloneNode(true))
    return parentSVG
  }

  function renderPNG() {
    var svg = cloneDrawing(2)
    // var xml = new XMLSerializer().serializeToString(svg)
    // var svgBase64 = btoa(xml)

    var p = svgToPNG(svg)
    p.then(function (pngBase64) {
      var img = document.createElement('img')
      img.src = pngBase64
      document.body.appendChild(img)
    })
    p.catch(function (error) {
      console.log(error)
    })
  }

  /*
    Event listeners
  */

  function documentResize(e) {
    computeSVGScale()
  }

  function mouseMove(e) {
    if (data.events.isDragging) {
      var p = eventPositionToSVGPosition(e)

      // dragging a label
      if (data.labels.element) {
        p.x -= data.labels.offset.x
        p.y -= data.labels.offset.y
        data.labels.element.setAttributeNS(null, 'transform', 'translate(' + p.x + ',' + p.y + ')')
      } else if (data.drawing.currentPath) {  // pencil icon dragging, continue drawing the path
        appendToPathBuffer(p)
        updateDrawingPath()
      }
    }
  }

  function mouseDown(e) {
    var p = eventPositionToSVGPosition(e)

    data.events.isDragging = true

    if (e.target.parentElement.classList.contains('draggable-label')) { // dragging a label
      data.labels.element = e.target.parentElement
      var m = data.labels.element.transform.baseVal[0].matrix
      data.labels.offset = { x: p.x - m.e, y: p.y - m.f }
    } else if (e.target.closest('.drawing-button')) { // clicking an interface button
      var buttonGroup = getSVGChild('user-interface-group')

      var pencilElement = null
      var eraserElement = null
      var undoElement = null

      for (var index = 0; index < buttonGroup.children.length; index++) {
        if (buttonGroup.children[index].classList.contains('drawing-button')) {

          if (buttonGroup.children[index].firstChild.classList.contains('pencil-button')) {
            pencilElement = buttonGroup.children[index].firstChild;
          }
          if (buttonGroup.children[index].firstChild.classList.contains('eraser-button')) {
            eraserElement = buttonGroup.children[index].firstChild;
          }
          if (buttonGroup.children[index].firstChild.classList.contains('undo-button')) {
            undoElement = buttonGroup.children[index].firstChild;
          }
        }
      }

      // Did the click happen on a user-interface button?
      // What button was it?
      if (e.target.closest('.pencil-button')) {
        data.interfaceButtons.selected.pencil = true
        data.interfaceButtons.selected.eraser = false

        pencilElement.setAttributeNS('', 'fill', '#baf7ff')
        eraserElement.setAttributeNS('', 'fill', '#ccc')

        // set the cursor
        data.svg.element.classList.add('pencil-cursor')
        data.svg.element.classList.remove('eraser-cursor')

        data.drawing.strokeColor = '#888'
        data.drawing.strokeWidth = 2;
      } else if (e.target.closest('.eraser-button')) {
        data.interfaceButtons.selected.pencil = false
        data.interfaceButtons.selected.eraser = true

        pencilElement.setAttributeNS('', 'fill', '#ccc')
        eraserElement.setAttributeNS('', 'fill', '#baf7ff')

        // set the cursor
        data.svg.element.classList.add('eraser-cursor')
        data.svg.element.classList.remove('pencil-cursor')

        data.drawing.strokeColor = '#fff'
        data.drawing.strokeWidth = 6;
      } else if (e.target.closest('.undo-button')) {
        undoElement.setAttributeNS('', 'fill', '#baf7ff')

        // briefly change color to acknowledge click
        setTimeout(() => {
          undoElement.setAttributeNS('', 'fill', '#ccc')
        }, 100);

        if(data.interfaceButtons.history.length > 0) { // must be past history in order to undo it.
          
          var lastAction = data.interfaceButtons.history.pop()
          if(lastAction === 'pencil' && data.drawing.drawnCounter >= 0) { // undo the last pencil action
            document.getElementById(data.svg.id + '-drawn-path-' + data.drawing.drawnCounter).remove()
            data.drawing.drawnCounter -= 1;
          } else if(lastAction === 'eraser' && data.drawing.erasedCounter >= 0) { // undo the last eraser action
            document.getElementById(data.svg.id + '-erased-path-' + data.drawing.erasedCounter).remove()
            data.drawing.erasedCounter -= 1;
          }
        }
      }
    } else { // not interface-related, drawing or erasing
      /*
        Erasing is implemented in a less-than-ideal way...drawing a white line on top of a white background.
        True erasing would take more time to implement and be more complicated.
      */
     if(data.interfaceButtons.selected.pencil) {
      data.interfaceButtons.history.push('pencil')
     } else if(data.interfaceButtons.selected.eraser) {
      data.interfaceButtons.history.push('eraser')
     }
      createAndInsertPath(e) // draw a new path / freehand line
    }

  }

  function mouseUp(e) {
    // reset properties
    data.events.isDragging = false
    data.labels.element = null

    data.drawing.currentPath = null
    data.drawing.initialPoint = null
    data.drawing.buffer = null
  }
}